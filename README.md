# installation

`~/miniconda3/bin/python3 -m pip install julia`


```python
import julia

# gave me a warning: `https://pyjulia.readthedocs.io/en/latest/troubleshooting.html`
julia.install()
```
