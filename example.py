from julia.api import Julia

jl = Julia(compiled_modules=False)

jl.eval("""include("foo.jl")""")

from julia.Main import MyModule

print(MyModule.func1(1))
print(MyModule.func2(2, 3))
